/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {
 if (a>b){
      return a;}
 else {return b;}

}

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {

    if (max2(a, b) > c) {
        return max2(a,b);
    } else {
        return c;
    }
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres (int n  ) {
    int i = 0;
    while (n>0) {
        n=n/10;
        i =i+1;
    }
    return i;
}

/**
 *
 * @param n pyramide compliquer finiiiii
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){
    return nbChiffres(n*n);
}



/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car) {
    for (int i = 0; i < nb; i++) {
        Ut.afficher(car);

    }
}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){
int nbEtoile=1;
    for (int i = 1; i<=h; i++){
repetCarac( h-i, ' ');
repetCarac(nbEtoile, c);
Ut.sautLigne();
    nbEtoile+=2;
    }

}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    while (nb1<=nb2){
        Ut.afficher(nb1%10 + " ");

    nb1++;
    }

}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
while (nb1<=nb2){
    Ut.afficher(nb2%10 + " ");


    nb2--;
}
}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {
    int nmbr= 1;
for (int i = 1; i <= h; i++) {
    repetCarac((h-i)*2,' ');
    afficheNombresCroissants(i , nmbr-1);

    afficheNombresDecroissants(i,nmbr);
    Ut.sautLigne();
    nmbr+=2;
}
}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c ){

    // action: retourne la racine carré de c du nombre entier
    int racine = (int) Math.sqrt(c);
    return ( c == racine * racine ? racine: -1);
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
// action: vérifie si la nb saisi est le carré d'un entier
    return ((racineParfaite(nb) != -1) ? true : false);
}

/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q) {
    int pNmrb = 0;
    int qNmrb = 0;
    for (int i = 1; i < p; i++) {
        if (p % i == 0) {
            pNmrb = pNmrb + i;

        }
    }
    for (int a = 1; a < q; a++) {
        if (q % a == 0) {
            qNmrb = qNmrb + a;

        }
    }
        if (pNmrb == q && qNmrb == p) {
              if(pNmrb==qNmrb){
                  return false ;
              }
              return true;
        }
        else {
                return false;
            }
        }

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){

    while (max<1){
        max=Ut.saisirEntier();
    }
for (int i = 1; i<=max; i++)
    for (int j= i+1 ; j<=max; j++ ){
        if (nombresAmicaux(i,j))
            Ut.afficherSL(i + "  "  + j);
    }
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2) {
    int hypothenuse = (int) Math.pow(c1, 2) + (int) Math.pow(c2, 2);

    return estCarreParfait(hypothenuse);
}

void main (){
    afficheAmicaux(5000);

}
